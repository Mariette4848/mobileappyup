import 'package:flutter/material.dart';
import 'package:yupagence/pages/login/login.dart';



class Welcome extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.fromLTRB(25, 100, 25, 25),
          child: Center(
            child: Column(
              children: <Widget>[
                Image.network(
                  'http://yup.africa/typo3temp/_processed_/csm_Yup_Bannieres_rectangulaire_846_x_553_px_Generique_ANG_cd6f759018.jpg',
                  width: 300,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 50),
                ),

                Text(
                  'YUP'.toUpperCase(),
                  style: TextStyle(
                    fontSize: 48,
                    color: Theme.of(context).primaryColor,
                    fontFamily: 'FiraCode',
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 30),
                ),
                Text(
                  'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard.',
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                  textAlign: TextAlign.center,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 30),
                ),
                MaterialButton(
                  onPressed: () {
                   Navigator.pushReplacement(
                     context,
                     MaterialPageRoute(
                       builder: (context) => LoginPage(),
                     ),
                   );
                  },
                  minWidth: double.infinity,
                  height: 50,
                  child: Text(
                    'Commencer'.toUpperCase(),
                    style: TextStyle(
                      fontFamily: 'FiraCode'
                    ),
                  ),
                  color: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}