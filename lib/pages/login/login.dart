import 'package:flutter/material.dart';
import 'package:yupagence/pages/widgets/roundedClipper.dart';
import 'package:yupagence/utils/Colors.dart';


class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  TextEditingController emailController;
  TextEditingController passwordController;

  @override
  void initState(){

    emailController = new TextEditingController();
    passwordController = new TextEditingController();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {


    // Email TextField Widget

    final email = TextFormField(
      controller: this.emailController,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
          fillColor: Color(0xffe60028),
          focusedBorder: OutlineInputBorder(
              borderSide: new BorderSide(
                  color: Colors.black54
              )
          ),
          border: OutlineInputBorder(),

          labelText: 'Votre Adresse Email',
          labelStyle: TextStyle(
              color: Colors.black54
          ),
//          errorText: emailFieldIsCorrect ? 'Email  est obligatoire': null
      ),
    );



    // password TextField Widget

    final password = TextFormField(
      controller: this.passwordController,
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(

          fillColor: Color(0xffe60028),
          focusedBorder: OutlineInputBorder(
              borderSide: new BorderSide(
                  color: Colors.black54
              )
          ),
          border: OutlineInputBorder(),

          labelText: 'Mot de passe',
          labelStyle: TextStyle(
              color: Colors.black54
          ),
//          errorText: passwordFieldIsCorrect ? 'le mot de passe  est obligatoire': null
      ),
    );

    // login widget button

    final loginButton = Padding(
      padding: const EdgeInsets.only(
          left: 30.0, right: 30.0, top: 10.0),
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).pushNamedAndRemoveUntil('/dashboard', (Route<dynamic> route) => false);
        },
        child: new Container(
            alignment: Alignment.center,
            height: 60.0,
            decoration: new BoxDecoration(
              color: redColorYup,
              borderRadius: new BorderRadius.circular(5.0)
            ),
            child: new Text(
              "Connexion".toUpperCase(),
              style: new TextStyle(
                  fontSize: 20.0, 
                  color: Colors.white,
                   fontFamily: 'FiraCode'
              )
            )
          ),
      ),
    );



    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              child: new ClipPath(
                clipper: RoundedClipper(),
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/images/background.png"),
                      fit: BoxFit.cover
                    )
                  ),
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(top: 170.0, bottom: 100.0),
                  child: Column(
                    children: <Widget>[

                    ],
                  ),
                ),
              ),
            ),
            Container(
              child: Padding(
                  padding: EdgeInsets.only(top: 20,left: 30,right: 30),
                child: email,
              ),
            ),
            Container(
              child: Padding(
                padding: EdgeInsets.only(top: 20,left: 30,right: 30),
                child: password,
              ),
            ),
            Container(
              child: Padding(
                padding: EdgeInsets.only(top: 20),
                child: loginButton,
              ),
            )


          ],
        ),
      ),
    );

  }
}