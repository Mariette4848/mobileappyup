import 'package:flutter/material.dart';
import 'package:yupagence/utils/Colors.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GridView.count(
          crossAxisCount: 2,
        padding: EdgeInsets.all(20),
        childAspectRatio: 8.0 / 9.0,
        children: <Widget>[
          buildCard("assets/images/localisation.png", '/addagence', 'localiser une agence'),
          buildCard("assets/images/help.png", '/addagence', 'Besion d\'aide'),
          buildCard("assets/images/contribution.png", '/addagence', 'Mes contributions'),
          buildCard("assets/images/list.png", '/addagence', 'Toute les agences')
        ],

      ),
      floatingActionButton: new FloatingActionButton(
        backgroundColor: redColorYup,
        child: Icon(
            Icons.add,
        ),
          onPressed: null
      ),
    );
  }

  Widget buildCard(String imagePath,String route,String title) {

    return GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamed(route);
      },
      child: Card(
          
          clipBehavior: Clip.antiAlias,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              AspectRatio(
                aspectRatio: 18.0 / 11.0,
                child: Image.asset(imagePath),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      title,
                      textAlign: TextAlign.center,
                      
                    ),
                  ],
                ),
              ),
          ],
        )
      ),
    );
  }
}