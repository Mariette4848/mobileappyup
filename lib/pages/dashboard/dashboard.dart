import 'package:flutter/material.dart';
import 'package:yupagence/pages/dashboard/home/homeDasboard.dart';
import 'package:yupagence/utils/Colors.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage>
  with SingleTickerProviderStateMixin{

  TabController controller;

  @override
  void initState() {
    controller = new TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(100.0),
        child: AppBar(
          title: Center(
            child: Padding(
              padding: const EdgeInsets.only(left: 50),
              child: new Image.asset(
                'assets/images/logo-yup.png', 
                height: 300,
                width: 400,
                fit: BoxFit.cover,
               
              ),
            ),
          ),

          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
                semanticLabel: 'search',
              ),

              onPressed: null,
            )
          ],
          backgroundColor: redColorYup,
          bottom: new TabBar(
            controller: controller,
            indicatorColor: Colors.white,
            tabs: <Widget>[
              new Tab(
                  text: 'Home',
              ),
              new Tab(
                  text: 'Paramètres',
              )
            ]
          ),
        ),
      ),

      body: new TabBarView(
        controller: controller,
        children: <Widget>[
          HomePage(),
          Text('home 3'),
        ],
      )

    );
  }
}