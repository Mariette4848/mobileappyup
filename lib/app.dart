import 'package:flutter/material.dart';
import 'package:yupagence/pages/dashboard/dashboard.dart';
import 'package:yupagence/pages/login/login.dart';
import 'package:yupagence/pages/welcome.dart';
import 'package:yupagence/utils/Colors.dart';


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Yup agence',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: redColorYup,
        accentColor: Color(0XFF233C63),
        fontFamily: 'Poppins',
      ),
      home: Welcome(),
      routes: <String, WidgetBuilder>{
        '/welcome': (BuildContext context) => Welcome(),
        '/login' : (BuildContext context) => LoginPage(),
        '/dashboard' : (BuildContext context) => DashboardPage(),
      },
    );
  }
}

